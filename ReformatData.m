%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Function to create expanded data and collapsed data structs %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [edata,cdata] = ReformatData(snps,ages,traits,covars)

% sizes
n = size(snps,1);
p = size(snps,2);
t = size(traits,2);
q = size(covars,2);

% structs
edata = [];
cdata = [];

% features
edata.F = zeros(n*t,3*p);
cdata.F = zeros(n,3*p);
for i = 1:n
    for k = 1:t
        ind = (i-1)*t + k;
        for g = 1:3
            edata.F(ind,g:3:end) = snps(i,:) == g-1;
            cdata.F(i,g:3:end) = snps(i,:) == g-1;
        end
    end
end

% snps
edata.X = zeros(n*t,p);
cdata.X = snps;
for i = 1:n
    for k = 1:t
        ind = (i-1)*t + k;
        edata.X(ind,:) = snps(i,:);
    end
end

% ages
edata.A = zeros(n*t,1);
cdata.A = mean(ages,2);
for i = 1:n
    for k = 1:t
        ind = (i-1)*t + k;
        edata.A(ind) = ages(i,k);
    end
end

% phenotypes
edata.Y = zeros(n*t,1);
cdata.Y = mean(traits,2);
for i = 1:n
    for k = 1:t
        ind = (i-1)*t + k;
        edata.Y(ind) = traits(i,k);
    end
end

% identities
edata.I = zeros(n*t,1);
cdata.I = (1:n)';
for i = 1:n
    for k = 1:t
        ind = (i-1)*t + k;
        edata.I(ind) = i;
    end
end

% covariates
edata.C = zeros(n*t,q);
cdata.C = covars;
for i = 1:n
    for k = 1:t
        ind = (i-1)*t + k;
        edata.C(ind,:) = covars(i,:);
    end
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%