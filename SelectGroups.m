%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Function to select optimal groups for Time-Varying Group SpAM %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [groups,norms,estimates] = SelectGroups(Y,S,X,C,lambda,maxgr,options)

% options
maxiter = options.maxIter;      % maximum number of iterations
tol = options.convgTol;         % stopping criterion
verbose = options.veryVerbose;  % verbose option

% parameters
n = length(Y);                  % number of data points
nG = size(X,2);                 % number of groups
dG = 3;                         % number of features per group
nF = dG*nG;                     % number of features
nC = size(C,2);                 % number of covariates

% initializations
f0 = zeros(n,1);                % fitted intercept function
fhat = zeros(n,nF);             % fitted function values at data points
chat = zeros(n,nC);             % fitted covariate values at data points
R = Y - f0;                     % residuals
epsilon = 1;                    % change since last iteration
iter = 0;                       % iteration counter

% calculate maximum norm
maxNorm = 0;
for j = 1:nG
    currNorm = 0;
    for g = 1:dG
        XInd = logical(X(:,j) == g-1);
        currS = S(XInd,XInd);
        rowSum = sum(currS,2);
        rowSum(rowSum == 0) = 1;
        currF = currS*R(XInd)./rowSum;
        if (sum(XInd) > 0)
            currNorm = currNorm + (norm(currF)^2)/sum(XInd);
        end
    end
    currNorm = sqrt(currNorm);
    if (currNorm > maxNorm)
        maxNorm = currNorm;
    end
end

% adjust lambda
lambda = lambda*maxNorm/sqrt(dG);

% fit model to training data
while (epsilon > tol && iter < maxiter)
    f0_old = f0;
    fhat_old = fhat;
    chat_old = chat;
    
    % fit intercept function on partial residual
    R = R + f0;
    f0 = S*R;
    R = R - f0;
    
    % fit group spam model on partial residual
    ind = randperm(nG);
    for j = 1:nG
        
        % function indices for current group
        groupInds = (ind(j)-1)*dG + (1:dG);

        % indicator for data points to use
        XInd = zeros(n,dG);
        for g = 1:dG
            XInd(:,g) = X(:,ind(j)) == g-1;
        end
        XInd = logical(XInd);

        % calculate current smoother matrix
        currentS = cell(dG,1);
        for g = 1:dG
            currentS{g} = S(XInd(:,g),XInd(:,g));
        end
        
        % update functions for current group
        R = R + sum(fhat(:,groupInds).*XInd,2); 
        fhat(:,groupInds) = EstimateGroup(groupInds,R,currentS,XInd,lambda,options);
        R = R - sum(fhat(:,groupInds).*XInd,2);

    end
    
    % fit linear model on partial residual
    if (nC > 0 && options.useCovars)
        
        % learn weights
        R = R + sum(chat,2);
        beta = regress(R,C);
        chat = C.*repmat(beta',n,1);
        R = R - sum(chat,2);
        
    end
    
    % prediction
    predY = sum(chat,2) + sum(fhat,2) + f0;
    err = mean((predY - Y).^2);

    % convergence test
    epsilon = max(sum(([f0 fhat chat]-[f0_old fhat_old chat_old]).^2,1)./(sum([f0_old fhat_old chat_old].^2,1)+1e-4));
    iter = iter + 1;
    
    % abort procedure if too many groups selected
    numgr = sum(sqrt(sum(fhat.^2)) > 0)/dG;
    if (iter > 10 && numgr > maxgr+10)
        break;
    end
    
    if (verbose)
        fprintf('\tBackfitting: At iteration %d, epsilon is %f, train MSE is %f\n',iter,epsilon,err);
    end
end

% compute norms
allNorms = zeros(1,nG);
for j = 1:nG
    groupInds = (j-1)*dG + (1:dG);
    currFhat = fhat(:,groupInds);
    for g = 1:dG
        XInd = X(:,j) == g-1;
        if (sum(XInd) > 0)
            allNorms(j) = allNorms(j) + sum(currFhat(XInd,g).^2)/sum(XInd);
        end
    end
    allNorms(j) = sqrt(allNorms(j));
end

% select nonzero features and groups
groups = find(allNorms > eps);
norms = allNorms(groups);

% save estimates
estimates.f0 = f0;
estimates.fhat = fhat;
estimates.chat = chat;
   
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%