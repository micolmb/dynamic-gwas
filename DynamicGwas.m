%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Performs GWAS using Time-Varying Group SpAM method %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function DynamicGwas(dataDir,outputDir,options)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Default Options %%

if ~exist('options','var')
    options = []; 
end

% file names & data options
if ~isfield(options,'snpFile')
    options.snpFile = 'snp_matrix.txt';         % snp data file
end
if ~isfield(options,'traitFile')
    options.traitFile = 'trait_matrix.txt';     % trait data file
end
if ~isfield(options,'ageFile')
    options.ageFile = 'age_matrix.txt';         % age data file
end
if ~isfield(options,'covarFile')
    options.covarFile = 'covar_matrix.txt';     % covariate data file
end
if ~isfield(options,'useCovars')
    options.useCovars = true;                   % include or exclude covariates
end
if ~isfield(options,'outputFile')
    options.outputFile = 'gwas_results.txt';    % gwas results file
end

% parameter selection options
if ~isfield(options,'paramSel')
    options.paramSel = 1;                       % parameter selection method
end
if ~isfield(options,'numSnps')
    options.numSnps = 10;                       % number of snps
end
if ~isfield(options,'lambdaVals')
    options.lambdaVals = 1:-0.1:0.1;            % values of lambda
end

% algorithmic options
if ~isfield(options,'kernelType')
    options.kernelType = 'gauss';               % kernel type
end
if ~isfield(options,'bandScaler')
    options.bandScaler = 1.5;                   % bandwidth scaler
end
if ~isfield(options,'maxIter')
    options.maxIter = 500;                      % max number of iterations
end
if ~isfield(options,'convgTol')
    options.convgTol = 5e-3;                    % tolerance for stopping criterion
end

% display & saving options
if ~isfield(options,'someVerbose')
    options.someVerbose = true;                 % display printouts for each lambda
end
if ~isfield(options,'veryVerbose')
    options.veryVerbose = false;                % display printouts for each iteration
end
if ~isfield(options,'plotResults')
    options.plotResults = false;                % plot fitted functions
end
if ~isfield(options,'savePlots')
    options.savePlots = false;                  % save plots of fitted functions
end
if ~isfield(options,'saveResults')
    options.saveResults = true;                 % save results to file
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load Data %%

% snps, traits, ages
snps = load(fullfile(dataDir,options.snpFile));
traits = load(fullfile(dataDir,options.traitFile));
ages = load(fullfile(dataDir,options.ageFile));

% get dimensions
n = size(traits,1);
t = size(traits,2);
p = size(snps,2);

% optional covariates
if (options.useCovars)
    covars = load(fullfile(dataDir,options.covarFile));
else
    covars = zeros(n,0);
end

% reformat data
data = ReformatData(snps,ages,traits,covars);
F = data.F; X = data.X; A = data.A; Y = data.Y; C = data.C;

% compute smoother matrix
S = GetSmoother(A,A,options);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Run Algorithm %%

% Option 1: find value of lambda that yields specified number of snps
if (options.paramSel == 1)
    
    gwasResults = struct('lambda',{},'snps',{},'weights',{},'estimates',{});
    numSnps = options.numSnps;
    
    minLambda = 0;
    maxLambda = 1;    
    i = 1;
    
    while (true)
        
        % set lambda
        lambda = (minLambda + maxLambda)/2;
        if (options.someVerbose)
            fprintf('\nRunning Time-Varying Group SpAM with lambda = %f\n',lambda);
        end
        
        % perform group selection
        [snps,weights,estimates] = SelectGroups(Y,S,X,C,lambda,numSnps,options);
        if (options.someVerbose)
            fprintf('Selected %d SNPs\n',length(snps));
        end
        
        % store results
        gwasResults(i).lambda = lambda;
        gwasResults(i).snps = snps;
        gwasResults(i).weights = weights;
        gwasResults(i).estimates = estimates;
        i = i + 1;
        
        % update lambda interval
        if (length(snps) == numSnps)
            break;
        elseif (length(snps) < numSnps)
            maxLambda = lambda;
        elseif (length(snps) > numSnps)
            minLambda = lambda;
        end
                
    end
    
% Option 2: run algorithm for each specified value of lambda
elseif (options.paramSel == 2)
    
    gwasResults = struct('lambda',{},'snps',{},'weights',{},'estimates',{});
    lambdaVals = sort(options.lambdaVals,'descend');    
    
    for i = 1:length(lambdaVals)

        % set lambda
        lambda = lambdaVals(i);
        if (options.someVerbose)
            fprintf('\nRunning Time-Varying Group SpAM with lambda = %f\n',lambda);
        end

        % perform group selection
        [snps,weights,estimates] = SelectGroups(Y,S,X,C,lambda,p,options);
        if (options.someVerbose)
            fprintf('Selected %d SNPs\n',length(snps));
        end
        
        % store results
        gwasResults(i).lambda = lambda;
        gwasResults(i).snps = snps;
        gwasResults(i).weights = weights;
        gwasResults(i).estimates = estimates;      
        
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot Results

if (options.plotResults)
    
    snps = gwasResults(end).snps;
    feats = reshape([(snps-1)*3+1;(snps-1)*3+2;(snps-1)*3+3],1,3*length(snps));
    estimates = EstimateFunctions(Y,A,S,F(:,feats),C,options);
    R = Y - estimates.f0 - sum(estimates.fhat,2) - sum(estimates.chat,2);
    norms = PlotFittedFunctions(X(:,snps),A,estimates.fhat,R,snps,options,...
        fullfile(outputDir,[options.outputFile(1:end-4) '.ps']));
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Save Results

if (options.saveResults)
    
    fid = fopen(fullfile(outputDir,options.outputFile),'w');
    fprintf(fid,'Lambda\tNumSnp\tSnpInds\tSnpWeights\n');
    for i = 1:length(gwasResults)
        lambda = gwasResults(i).lambda;
        numSnp = length(gwasResults(i).snps);
        snps = sprintf('%d,',gwasResults(i).snps);
        weights = sprintf('%.3f,',gwasResults(i).weights);
        fprintf(fid,'%f\t%d\t%s\t%s\n',lambda,numSnp,snps(1:end-1),weights(1:end-1));
    end
    fclose(fid);
    
    save(fullfile(outputDir,[options.outputFile(1:end-4) '.mat']),'gwasResults');
    
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%