This repository contains MATLAB code to run Dynamic GWAS using a method called Time-Varying Group SpAM. The main function is DynamicGwas.m, and its usage is described below.

DynamicGwas(dataDir,outputDir,options)
-- Specify dataDir, a directory containing tab-delimited data files of SNP, trait, age, and (optionally) extra covariate values
-- Specify outputDir, a directory in which to store the results
-- Can also pass in an options struct, with the following fields:
	snpFile: [default is snp_matrix.txt] name of file in dataDir containing SNP data
	traitFile: [default is trait_matrix.txt] name of file in dataDir containing trait data
	ageFile: [default is age_matrix.txt] name of file in dataDir containing age data
	covarFile: [default is covar_matrix.txt] name of file in dataDir containing covariate data
	useCovars: [default is true] indicator for whether to use extra covariates in model
	outputFile: [default is gwas_results.txt] name of file in outputDir in which to store results
	paramSel: [default is 1] if 1, searches for a value of lambda that yields a certain number of SNPs (specified in options.numSnps); if 2, runs method for each value of lambda in a set (specified in options.lambdaVals)
	numSnps: [default is 10] number of SNPs to select; only used if options.paramSel = 1
	lambdaVals: [default is 0.1:0.1:10] vector of lambda values to try; only used if options.paramSel = 2
	kernelType: [default is gauss] type of kernel to use for smoother matrix
	bandScaler: [default is 1.5] scaler applied to bandwidth used for smoother matrix
	maxIter: [default is 500] maximum number of backfitting iterations
	convgTol: [default is 5e-3] tolerance for backfitting stopping criterion
	someVerbose: [default is true] indicator for whether to display printouts for each value of lambda
	veryVerbose: [default is false] indicator for whether to display printouts for each back fitting iteration
	plotResults: [default is false] indicator for whether to plot fitted functions for last value of lambda
	savePlots: [default is false] indicator for whether to save the plots of fitted functions; only applies if plotResults is set to true
	saveResults: [default is true] indicator for whether or not to save the results to a file
 
Here is some additional information on the expected formatting of data files:
-- snpFile: matrix of SNP genotypes; should be a tab- or comma-delimited file with one row per individual and one column per SNP; each entry should be an integer indicating the number of copies of the minor allele
-- traitFile: matrix of trait values; should be a tab- or comma-delimited file with one row per individual and one column per time point; each entry should be a real number that encodes the trait value
-- ageFile: matrix of age measurements; should be a tab- or comma-delimited file with one row per individual and one column per time point; each entry should be a real number that encodes the age value
-- covarFile: matrix of covariate values; should be a tab- or comma-delimited file with one row per individual and one column per covariate; each entry should be a real number that encodes the covariate value; covariates can be continuous variables or indicator variables