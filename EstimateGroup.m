%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Function to run sub-routine of Time-Varying Group SpAM backfitting in 
%% which component functions are estimated for one group of variables %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [fhat,curnorm] = EstimateGroup(currGroupInds,R,S,XInd,lambda,options)

% options
maxiter = options.maxIter;      % maximum number of iterations
tol = options.convgTol;         % stopping criterion

% parameters
n = size(R,1);                  % number of data points
dG = length(currGroupInds);     % size of group

% initializations
QR = zeros(n,1);
curnorm = 0;

% compute the norm and form the matrices for equations
for g = 1:dG
    rowSum = sum(S{g},2);
    rowSum(rowSum == 0) = 1;
    P = S{g}*R(XInd(:,g))./rowSum;
    QR(XInd(:,g)) = P;
    n_g = sum(XInd(:,g));
    if (n_g > 0)
        curnorm = curnorm + (norm(P)^2)/n_g;
    end
end
curnorm = sqrt(curnorm);

% estimate the component functions
if (curnorm <= lambda*sqrt(dG)) % set all functions to zero
    fhat = zeros(n,dG);
else % solve equation by fixed point iteration
    iter = 0;
    epsilon = 1;
    fhat = rand(n,1)*sqrt(dG);
    while (epsilon > tol && iter < maxiter)
        fhat_old = fhat;
        fhat_norm = 0;
        for g = 1:dG
            n_g = sum(XInd(:,g));
            if (n_g > 0)
                fhat_norm = fhat_norm + sum(fhat(XInd(:,g)).^2)/n_g;
            end
        end
        fhat_norm = sqrt(fhat_norm);
        fhat = QR./(1 + eps + lambda*sqrt(dG)/fhat_norm);
        epsilon = max(abs(fhat_old - fhat));
        iter = iter + 1;
    end
    fhatFull = zeros(n,dG);
    for g = 1:dG
        ind = XInd(:,g);
        fhatFull(ind,g) = fhat(ind);
        fhatFull(ind,g) = fhatFull(ind,g) - mean(fhatFull(ind,g));
    end
    fhat = fhatFull;
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%