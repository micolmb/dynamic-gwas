%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Function to estimate all functions of Time-Varying Additive Model %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [estimates] = EstimateFunctions(Y,A,S,F,C,options)
    
% options
maxiter = options.maxIter;      % maximum number of iterations
tol = options.convgTol;         % stopping criterion
verbose = options.veryVerbose;  % verbose option

% parameters
n = size(F,1);                  % number of data points
nF = size(F,2);                 % number of features
nC = size(C,2);                 % number of covariates

% initializations
f0 = zeros(n,1);                % fitted intercept function
fhat = zeros(n,nF);             % fitted values at training data points
chat = zeros(n,nC);             % fitted values at training data points
R = Y - f0;                     % residuals
epsilon = 1;                    % change since last iteration
iter = 0;                       % iteration counter

% fit model to training data
while (epsilon > tol && iter < maxiter)
    f0_old = f0;
    fhat_old  = fhat;
    chat_old = chat;
    
    % fit intercept function on partial residual
    R = R + f0;
    f0 = S*R;
    R = R - f0;
    
    % fit additive model on partial residual
    for f = 1:nF    
        
        % indicator for data points to use
        FtrainInd = logical(F(:,f));
        
        % calculate current smoother matrix
        currentS = S(FtrainInd,FtrainInd);
        rowSum = sum(currentS,2);
        rowSum(rowSum == 0) = 1;
        
        % update current function
        R = R + fhat(:,f).*FtrainInd;
        partialR = R(FtrainInd);
        fhat(FtrainInd,f) = (currentS*partialR)./rowSum;
        R = R - fhat(:,f).*FtrainInd;       
        
    end
    
%     for f = 1:nF
%         figure; hold on;
%         FtrainInd = find(F(:,f));
%         [V,I] = sort(A(FtrainInd));
%         plot(V,fhat(FtrainInd(I),f),'LineWidth',5);
%         plot(V,R(FtrainInd(I)) + fhat(FtrainInd(I),f),'.k','LineWidth',2);
%     end
        
    % fit linear model on partial residual
    if (nC > 0 && options.useCovars)
        
        % learn weights
        R = R + sum(chat,2);
        beta = regress(R,C);
        chat = C.*repmat(beta',n,1);
        R = R - sum(chat,2);
        
    end
    
    % prediction    
    predY = sum(fhat,2) + f0;
    err = mean((predY - Y).^2);
    
    % convergence test
    epsilon = max(sum(([f0 fhat chat]-[f0_old fhat_old chat_old]).^2,1)./(sum([f0_old fhat_old chat_old].^2,1)+1e-4));
    iter = iter + 1;

    if (verbose)
        fprintf('\tBackfitting: At iteration %d, epsilon is %f, train MSE is %f\n',iter,epsilon,err);
    end
end

% save estimates
estimates.f0 = f0;
estimates.fhat = fhat;
estimates.chat = chat;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%