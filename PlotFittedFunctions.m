%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Function to plot fitted functions of grouped nonparametric regression %%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function fnorms = PlotFittedFunctions(X,A,f,R,labels,options,outputFile)

% sizes
n = size(f,1);
nF = size(f,2);
dG = 3;
nG = nF/dG;

% plot settings
colorstring = 'rgb';
legendstring = {'AA','Aa','aa'};

% axis limits
maxAge = ceil(max(A));
minVal = (min(f(:)) + min(R));
maxVal = (max(f(:)) + max(R));

fnorms = zeros(nG,1);

% plot functions
for j = 1:nG
    
    h = figure; hold on;
    set(gcf,'Position',[0   0   900   375])
    set(h,'PaperPositionMode','auto');
    orient(h,'landscape');
    set(gcf,'Color','w');
    
    fnorm = zeros(dG,1);
    for g = 1:dG
        a = subplot(1,3,g); hold on;
        fInd = (j-1)*dG + g;       
        XInd = find(X(:,j)==g-1);
        if ~isempty(XInd)
            fnorm(g) = sum(f(:,fInd).^2)/length(XInd);
        end
        [V,I] = sort(A(XInd));
        title(sprintf('Genotype [%s], Norm [%.03f]',legendstring{g},sqrt(fnorm(g))),'FontSize',14);
        %title(['Genotype [' legendstring{g} '], Norm = ' num2str(sqrt(fnorm(g)))],'FontSize',14);        
        xlim([0 maxAge]); ylim([minVal maxVal]);
        set(a,'FontSize',14);
        xlabel('$t$','Fontsize',15,'interpreter','latex');
        ylabel(['$f_{' num2str(j) '}^' num2str(g-1) '(t)$'],'Fontsize',15,'interpreter','latex');
        plot(V,R(XInd(I)) + f(XInd(I),fInd),'.k','Linewidth',5);
        plot(V,f(XInd(I),fInd),[colorstring(g) '-'],'LineWidth',5);
    end

    fnorms(j) = sqrt(sum(fnorm));

    suptitle(['Fitted Functions, Group ' num2str(labels(j)) ', Norm = ' num2str(fnorms(j))]);
    
    if (options.savePlots)
        print(h,outputFile,'-dpsc','-append');
    end

end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%